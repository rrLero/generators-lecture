let nums = {
    [Symbol.iterator]: function*() {
        yield* [1, 2, 3];
        yield 4;
        yield* [5, 6]
    }
};


console.log([...nums]);

