let roman = {
    [Symbol.iterator]: function*() {
        yield 'R';
        yield 'o';
        yield 'm';
        yield 'a';
        yield 'n';
    }
};

for (let l of roman) {
    console.log(l);
}
