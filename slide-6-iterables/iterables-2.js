let roman = {
    [Symbol.iterator]: function*() {
        yield* 'Roman';
    }
};

for (let l of roman) {
    console.log(l);
}
