export function isPromise(x) {
    return x && typeof x.then === 'function';
}

export function wait(time, res) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(res);
        }, time);
    });
}