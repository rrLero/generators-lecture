export function fetchPlaceSummaries(input) {
    return fetch(`http://localhost:3001/placeDetails?description_like=${input}`)
        .then(response => response.json())
        .then(jsonData => {
            return jsonData;
        });
}

export function fetchPlaceDetails(placeids) {
    return Promise.all(
        placeids.map(placeid => {
            return fetch(`http://localhost:3001/placeSummary?placeId=${placeid}`)
                .then(response => response.json())
                .then(jsonData => jsonData[0]);
        })
    );
}