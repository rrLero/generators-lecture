import React, {useState} from 'react';
import {autocomplete} from './autocomplete';
import {PlaceSearchResultList} from './place-search-result-list';

export const PlaceSearchContainer = () => {

    const [term, setTerm] = useState('');
    const [results, setResults] = useState([]);
    const [inProgress, setInProgress] = useState(false);

    const beginSearch = async (term) => {
        setTerm(term);
        setInProgress(true);
        let results = await autocomplete(term);
        setResults(results);
        setInProgress(false);
    };

    return (
        <PlaceSearchResultList
            results={results}
            inProgress={inProgress}
            term={term}
            onSearchTermChanged={beginSearch}/>
    );
};
