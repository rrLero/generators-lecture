import {fetchPlaceSummaries, fetchPlaceDetails} from './utils/places';
// import {wait} from './utils/promise';

export const autocomplete = async term => {

    console.log(`⏳ Beginning search for ${term}`);

    let placeResults = await fetchPlaceSummaries(term);
    console.log(`✅ Completed search for ${term}`);

    let placeIds = placeResults.map(place => place.placeId);
    let places = await fetchPlaceDetails(placeIds);
    console.log(places);

    return places;

    // return term === 'duis' ? wait(2500, places) : places;
};


