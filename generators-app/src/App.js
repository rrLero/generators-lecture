import * as React from 'react';
import { PlaceSearchContainer } from './withoutGenerators/place-search-container';

export const App = () => {
  return (
      <PlaceSearchContainer/>
  );
};

export default App;
