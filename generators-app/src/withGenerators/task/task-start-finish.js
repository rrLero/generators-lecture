import {isPromise, wait} from '../utils/promise';

export const task = (genFn) => {
    let p = new Promise((resolve) => {
        let it = genFn(); // Get the iterator
        // debugger;
        let value;
        const nextStep = (lastVal) => {
            const result = it.next(lastVal);
            if (result.done && !result.value) {
                resolve(value);
                return;
            } else {
                // debugger;
                value = result.value;
                if (isPromise(result.value)) {
                    result.value.then(res => {
                        nextStep(res);
                    })
                } else {
                    nextStep(result.value);
                }
            }
        };

        nextStep('begin');

    });

    return p;
};

task(function* () {
    let first = yield wait(500).then(() => 'FIRST');
    console.log('first', first);
    let second = yield wait(500).then(() => 'SECOND');
    console.log('second', second);

    let third = yield 6;
    console.log('third', third);
    // return 'third';
}).then(lastVal => {
    console.log('------->', lastVal);
});