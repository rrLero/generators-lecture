import {isPromise, wait} from '../utils/promise';

export const task = (genFn) => {
    let p = new Promise((resolve) => {
        let it = genFn(); // Get the iterator

    });

    return p;
};

task(function* () {
    let first = yield wait(500).then(() => 'FIRST');
    console.log('first', first);
    let second = yield wait(500).then(() => 'SECOND');
    console.log('second', second);

    let third = yield 6;
    console.log('third', third);
    // return 'third';
}).then(lastVal => {
    console.log('------->', lastVal);
});