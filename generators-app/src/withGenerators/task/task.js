import {isPromise, wait} from '../utils/promise';

export const task = (genFn) => {
    let p = new Promise((resolve) => {
        let it = genFn(); // Get the iterator
        let value;

        const nextStep = lastPromiseVal => {
            let itResult = it.next(lastPromiseVal);
            if (itResult.done && typeof itResult.value === 'undefined') {
                resolve(value);
                return;
            } else {
                value = itResult.value;
                if (isPromise(value)) {
                    value.then((promiseResult) => {
                        if (!p.cancelled)
                            nextStep(promiseResult);
                    });
                    // value is a promise
                } else {
                    // value is not a promise
                    nextStep(value);
                }
            }
        };

        nextStep(undefined); // just the first time, this is ok
    });
    p.cancelled = false;
    return p;
};

// TEST with VS Code "code runner extension"
task(function* () {
    let first = yield wait(500).then(() => 'FIRST');
    console.log('first', first);
    let second = yield wait(500).then(() => 'SECOND');
    console.log('second', second);
    return 'third';
}).then(lastVal => {
    console.log('------->', lastVal);
});