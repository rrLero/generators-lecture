import React, {useState} from 'react';
import {autocomplete} from './autocomplete';
import {PlaceSearchResultList} from './place-search-result-list';

export const PlaceSearchContainer = () => {

    const [term, setTerm] = useState('');
    const [results, setResults] = useState([]);
    const [existingSearch, setExistingSearch] = useState(undefined);

    const beginSearch = (term) => {

        if (existingSearch) {
            existingSearch.cancelled = true;
        }

        let p = autocomplete(term);

        setTerm(term);
        setExistingSearch(p);
        setResults([]);

        p.then((results) => {
            setResults(results);
            setExistingSearch(undefined);
        });
    };

    return (
        <PlaceSearchResultList
            results={results}
            inProgress={!!existingSearch && results.length === 0}
            term={term}
            onSearchTermChanged={beginSearch}/>
    );
};
