import {wait} from './utils/promise';
import {task} from './task';
import {fetchPlaceSummaries, fetchPlaceDetails} from './utils/places';

export const autocomplete = (term) => {
    return task(function* () {

        console.log(`⏳ Beginning search for ${term}`);

        let placeResults = yield fetchPlaceSummaries(term);
        console.log(`✅ Completed search for ${term}`);
        let placeIds = placeResults.map(place => place.placeId);
        let places = yield fetchPlaceDetails(placeIds, term === 'duis');
        console.log(places);

        return places;
    });
};

