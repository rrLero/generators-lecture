import * as React from 'react';
import {PlaceSearchResult} from './place-search-result';

export const PlaceSearchResultList =
    ({onSearchTermChanged, term, inProgress, results}) => {
        let searchResults = null;
        if (term.length === 0) {
            searchResults = [
                <li key='goahead'>Type something in above to search</li>
            ];
        } else if (inProgress) {
            searchResults = [
                <li key='searching' className="blue">Searching for {term}...</li>
            ];
        } else if (results.length === 0) {
            searchResults = [
                <li key='noresults' className="red">Sorry! No results for {term}.</li>
            ];
        } else {
            let i = 0;
            searchResults = results.map(r => (
                <PlaceSearchResult key={r.placeId} {...r} />
            ));
        }
        return (
            <div>
                <h2>Search for a place</h2>
                <input type="search" placeholder="Search"
                       onChange={e => onSearchTermChanged ? onSearchTermChanged(e.target.value) : () => {
                       }}/>
                <ul className="results">
                    {searchResults}
                </ul>
            </div>
        );
    };

