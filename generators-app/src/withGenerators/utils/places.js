import {wait} from './promise';

export function fetchPlaceSummaries(input) {
    return fetch(`http://localhost:3001/placeDetails?description_like=${input}`)
        .then(response => response.json())
        .then(jsonData => {
            return jsonData;
        });
}

export function fetchPlaceDetails(placeids, isWait) {
    return isWait ? wait(2500, Promise.all(
        placeids.map(placeid => {
            return fetch(`http://localhost:3001/placeSummary?placeId=${placeid}`)
                .then(response => response.json())
                .then(jsonData => jsonData[0]);
        }))) : Promise.all(
        placeids.map(placeid => {
            return fetch(`http://localhost:3001/placeSummary?placeId=${placeid}`)
                .then(response => response.json())
                .then(jsonData => jsonData[0]);
        })
    );
}