function* fib() {
    let lastLast = 1;
    let last = 0;
    while (true) {
        let val = last + lastLast;
        yield val;
        last = lastLast;
        lastLast = val;
    }
}

for (let x of fib()) {
    console.log(x);
}
