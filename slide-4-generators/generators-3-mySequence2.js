function* mySequence() {
    yield 'JS';
    yield 'MeetUp';
    return 'Ok';
    yield 'Want More';
}

const it = mySequence();

console.log(it.next());
console.log(it.next());
console.log(it.next());
console.log(it.next());
console.log(it.next());
console.log(it.next());
console.log(it.next());
