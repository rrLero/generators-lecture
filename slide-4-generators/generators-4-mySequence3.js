function* mySequence() {
    yield 'JS';
    yield 'MeetUp';
    return 'Want More';
    console.log('Again me');
}

const it = mySequence();

// console.log(it.next());
// console.log(it.next());
// console.log(it.next());
// console.log(it.next());
// console.log(it.next());
// console.log(it.next());
// console.log(it.next());

for (let x of it) {
    console.log(`> ${x}`)
}
console.log(it.next());
console.log(it.next());
console.log(it.next());