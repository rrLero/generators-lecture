function* mySequence() {
    let power = 1;
    while (true) {
        console.log('before Yield');
        yield Math.pow(2, power++);
        console.log('after Yield')
    }
}

const it = mySequence();

console.log(it.next());
console.log(it.next());
console.log(it.next());
