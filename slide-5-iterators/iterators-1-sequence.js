function* sequence() {
    let last = 0;

    while(true) {
        last = yield last + 5;
        console.log(`last=${last}`)
    }
}

let it = sequence();

console.log(it.next().value);
console.log(it.next(35).value);
console.log(it.next(100).value);
console.log(it.next().value);

