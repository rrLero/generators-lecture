let roman = {
    _name: 'Roman',
    [Symbol.iterator]() {
        let i = 0;
        let str = this._name;
        return {
            next() {
                if (i < str.length) {
                    return {done: false, value: str[i++]};
                }
                return {done: true};
            }
        }
    }
};

for (let l of roman) {
    console.log(l);
}